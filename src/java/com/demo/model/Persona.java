
package com.demo.model;


public class Persona {
    
    private String Nombre;
    private String Apellido;
    private String Direccion;
    private String Sexo;
    private String Telefono;
    private int Codigo;
    
    
    public Persona (){}

        
    
    public String getNombre() {
        return Nombre;
    }

   
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    
    public String getApellido() {
        return Apellido;
    }

    
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    
    public String getDireccion() {
        return Direccion;
    }

   
    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

   
    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    
    public String getTelefono() {
        return Telefono;
    }

    
    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    
    public int getCodigo() {
        return Codigo;
    }

    
    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    
    
    

    
}
