
package com.demo.dao;

import com.demo.model.Persona;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;



public class personadao extends dao{
    
  
    
    //METODO REGISTRAR
    
    public void registrar (Persona per) throws Exception {
    
    try {
        this.Conectar();
        PreparedStatement at = this.getCn().prepareStatement("INSERT INTO personabd (Nombre, Apellido, Sexo, Telefono, Direccion) VALUES (?,?,?,?,?)");
        at.setString(1, per.getNombre());
        at.setString(2, per.getApellido());
        at.setString(3, per.getSexo());
        at.setString(4, per.getTelefono());
        at.setString(5, per.getDireccion());
        at.executeUpdate();
    }catch(Exception e){
    
    throw e;
    }finally {
    this.Cerrar();
    }
    } 
    
    //********************************************************************
    
    
    //METODO LISTAR
    public List<Persona> listar () throws Exception{
    List<Persona> Lista;
    ResultSet rs; 
    try{
    this.Conectar();
    PreparedStatement at = this.getCn().prepareCall("SELECT Codigo, Nombre, Apellido, Sexo, Telefono, Direccion FROM personabd");
    rs = at.executeQuery();
    Lista = new ArrayList();
    while(rs.next()){
    Persona per= new Persona();
    per.setCodigo(rs.getInt("Codigo"));
    per.setNombre(rs.getString("Nombre"));
    per.setApellido(rs.getString("Apellido"));
    per.setSexo(rs.getString("Sexo"));
    per.setTelefono(rs.getString("Telefono"));
    per.setDireccion(rs.getString("Direccion"));
    
    Lista.add(per);
    }
    }catch(Exception e){
    throw e;
    }finally{
        this.Cerrar();
    }
    return Lista;
    } 
        
    //******************************************************************
    
    
    //METODO LEER
    
    public Persona readID (Persona per) throws Exception {
       Persona pers = null;
       ResultSet rs;
     try{
    this.Conectar();
    PreparedStatement at = this.getCn().prepareStatement("SELECT  Codigo, Nombre, Apellido, Sexo, Telefono, Direccion FROM personabd WHERE Codigo = ? ");
       
     at.setInt(1, per.getCodigo());
   
    rs = at.executeQuery();
        while(rs.next()){
        pers = new Persona ();
        pers.setCodigo(rs.getInt("Codigo"));
        pers.setNombre(rs.getString("Nombre"));
        pers.setApellido(rs.getString("Apellido"));
        pers.setSexo(rs.getString("Sexo"));
        pers.setTelefono(rs.getString("Telefono"));
        pers.setDireccion(rs.getString("Direccion"));
        
        }
    }catch(Exception e){
    throw e;
    }finally{
        this.Cerrar();
    }
    return pers;
    } 
    
    //*******************************************************************
    
    //METODO MODIFICAR
    
    public void modificar (Persona per) throws Exception {
    
    try {
        this.Conectar();
        PreparedStatement at = this.getCn().prepareStatement("UPDATE personabd SET Nombre = ?, Apellido = ?, Sexo = ?, Telefono = ?, Direccion = ? WHERE Codigo = ?");
        at.setString(1, per.getNombre());
        at.setString(2, per.getApellido());
        at.setString(3, per.getSexo());
        at.setString(4, per.getTelefono());
        at.setString(5, per.getDireccion());
        at.setInt(6, per.getCodigo());
        at.executeUpdate();
    }catch(Exception e){
    
    throw e;
    }finally {
    this.Cerrar();
    }
    } 
    
    //**********************************************************************
   
     //METODO ELIMINAR
    
    
    public void eliminar (Persona per) throws Exception {
    
    try {
        this.Conectar();
        PreparedStatement at = this.getCn().prepareStatement("DELETE FROM personabd WHERE Codigo = ?");
        
        at.setInt(1, per.getCodigo());
        
        at.executeUpdate();
    }catch(Exception e){
    
    throw e;
    }finally {
    this.Cerrar();
    }
    } 
    
    //************************************************************
}
