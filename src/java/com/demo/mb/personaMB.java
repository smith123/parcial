
package com.demo.mb;

import com.demo.dao.personadao;
import com.demo.model.Persona;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;



@ManagedBean
@ViewScoped

public class personaMB {

    private Persona persona = new Persona();
   private List<Persona> lstpersonas;
   
    public personaMB() {
    }

    
    public Persona getPersona() {
        return persona;
    }

    
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public List<Persona> getLstpersonas() {
        return lstpersonas;
    }

    
    public void setLstpersonas(List<Persona> lstpersonas) {
        this.lstpersonas = lstpersonas;
    }
    
    
    
    
    
    
    //METODO REGISTRAR
    
    public void registrar() throws Exception {
        personadao dao;
    try{
    dao=new personadao();
    dao.registrar(persona);
    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"REGISTRO EXITOSO!", "Contacto Guardado."));
    }catch(Exception e){
    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"ERROR!!", null));
    
    }finally {
    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    }
    }
    
    //...........................................
    
    
    //METODO LISTAR
    
    public void listar() throws Exception {
        personadao dao;
    try{
    dao=new personadao();
    lstpersonas=dao.listar();
    }catch(Exception e){
        throw e;
    
    }
    }
    
   //.................................................
    
    
    //METODO LEER
    
   public void readID(Persona per) throws Exception {
        personadao dao;
        Persona temporal;
    try{
    dao=new personadao();
    temporal = dao.readID(per);
    if(temporal != null){
    this.persona = temporal;
    }
    }catch(Exception e){
        throw e;
    
    }
     }
    
   //......................................................
    
   //METODO MODIFICAR
   
   public void modificar () throws Exception {
        personadao dao;
    try{
    dao=new personadao();
    dao.modificar(persona);
    this.listar();
    }catch(Exception e){
        throw e;
    
    }
    }
   //..................................................
   
   //METODO ELIMINAR
   public void eliminar(Persona per) throws Exception {
        personadao dao;
    try{
    dao=new personadao();
    dao.eliminar(per);
    this.listar();
    }catch(Exception e){
        throw e;
    
    }
    }
   
   //.......................................................
}